sifir = int(input("Cetak sifir berapa?: "))
nombor = [1,2,3,4,5,6,7,8,9,10,11,12]

for i in nombor:
    jawapan = i * sifir
    print(f"{i} x {sifir} = {jawapan}")
    
myrange = range(1,100)
print(f"\nCetakan sifir {sifir} menggunakan function range:")

for i in myrange:
    jawapan = i * sifir
    print(f"{i} x {sifir} = {jawapan}")