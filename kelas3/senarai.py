# List kosong
list_kosong = []

# List sejenis e.g. integer
list_nomborbulat = [1, 2, 3, 4, 5]

# List pelbagai
list_campuran = ["a", 9, 3.14]


print(list_nomborbulat)

# access item in list
# using index
print(list_nomborbulat[1])

#using -ve index
print(list_nomborbulat[-2])

#using slice
print(list_nomborbulat[1:4])


#changing the list
#tambah single item
list_nomborbulat.append(7)
print(list_nomborbulat)

#tambah dengan list
list_nomborbulat.extend([8,9,10])
print(list_nomborbulat)

#tambah dengan menyelit ditengah
list_nomborbulat.insert(5,6)
print(list_nomborbulat)

#deleting
myword = ['p','r','o','x','b','b','l','e','m']
print(myword)
myword.remove('b')
print(myword)
del myword[3]
print(myword)

#ambil satu
#dari hujung
ambilan = myword.pop()
print(ambilan)
print(myword)

#guna index
ambilan = myword.pop(2)
print(ambilan)
print(myword)

#other function
print(f"jumlah character dalam variable myword adalah {len(myword)}")