nama = "Abu"
sapaan = "Selamat datang Abu"

print(sapaan)

nama = "Ali"

print(sapaan)

sapaan = f"Selamat datang {nama}"
print(sapaan)

nama = "Ahmad"
print(sapaan)

print(f"Selamat datang {nama}")
nama = "Arif"
print(f"Selamat datang {nama}")