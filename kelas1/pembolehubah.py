# -- Strings --

nama = "Abu"
name = "Tausi"

print(nama)
print(nama * 2)

nama = nama + "test"
print(nama)

# -- Nombor 'integer' dan nombor 'floats' --

x = 20
harga = 19.99

diskaun = 0.15

harga_jualan = harga * (1 - diskaun)

print(harga_jualan)

# -- mengubah pembolehubah --
# Pembolehubah (variables) adalah nama untuk data dalam ruang memori.

a = 50
b = a

# di sini, kita tetapkan data '50' dengan nama 'a' dan 'b'.

print(a)
print(b)

b = 12

# di sini, kita tetapkan data '12' dengan nama 'b'. Ruang memori yang diberi nama 'a' masih kekal dengan data '50'!

print(a)
print(b)